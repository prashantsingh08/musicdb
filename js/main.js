var musicDB = new musicdb;

function musicdb() {
    this.init = function() {
        this.search();        
    }

    // Searcher
    this.search = function() {
        var $this = this;
        var form = document.querySelector('#form');
        form.addEventListener('submit', function(event) {
            event.preventDefault();
            var value =document.querySelector('#input-search').value;
            form.reset();
            $this.getData(value);
        })
    }

    // Requesting iTunes API for artist
    this.getData = function(artist) {
        var $this = this;
        var http = new XMLHttpRequest();
        var url = 'https://itunes.apple.com/search?term='+ artist +'&entity=album';
        var method = 'GET';
        var container = document.querySelector('#album-list-container');
        container.innerHTML = '';
        http.open(method, url);
        http.onreadystatechange = function() {
            if(http.readyState === XMLHttpRequest.DONE && http.status === 200) {
                $this.showArtist(JSON.parse(http.response));
            } else if(http.readyState === XMLHttpRequest.DONE && http.status !== 200) {
                // Nothing to do
                // Functionality defined through notMatch
            }
        }
        http.send();
    }

    // Display Artist
    this.showArtist = function(obj) {
        var container = document.querySelector('#album-list-container');
        var notMatch = document.querySelector('#not-match');
        var template = '';
        if(obj.results.length > 0) {
            notMatch.style.display = 'none';
            for(var i = 0; i < obj.results.length; i++) {
                template += '<div class="col-sm-3 album-item">';
                template +=     '<div class="item-thmb" style="background: url('+ obj.results[i].artworkUrl100 +')"></div>';
                template +=     '<div class="item-title">'+ obj.results[i].collectionName +'</div>';
                template +=     '<div class="item-price">';
                template +=         '<span>Price: </span>'+ obj.results[i].collectionPrice +' '+ obj.results[i].currency;
                template +=     '</div>';
                template +=     '<a href="'+ obj.results[i].collectionViewUrl +'" target="_blank">Buy Now</a>';
                template += '</div>';
            }
            container.innerHTML = '';
            container.insertAdjacentHTML('afterbegin', template);
        } else {
            notMatch.style.display = 'block';
        }
    }

    this.init();
}